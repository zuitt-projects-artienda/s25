//1
db.fruits.aggregate([
	{
		$match: {"supplier": "Red Farms Inc."}
	},
	{
		$count: "itemsInRedFarms"
	}
	])


//2
db.fruits.aggregate([
	{
		$match: {"price" : {$gt: 50}}
	},
	{
		$count: "noOfItemsGreaterThan50"
	}
	])


//3
db.fruits.aggregate([
	{
		$match: {"onSale": true} 
	},
	{
		$group: {_id: "$supplier", avgPricePerSupplier: {$avg: "$price"}}
	}
])


//4
db.fruits.aggregate([
	{
		$match: {"onSale": true}
	},
	{
		$group: {_id: "$supplier", maxPricePerSupplier: {$max: "$price"}}
	}
	])

//5
db.fruits.aggregate([
	{
		$match: {"onSale": true}
	},
	{
		$group: {_id: "$supplier", minPricePerSupplier: {$min: "$price"}}
	}
	])